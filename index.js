const express = require("express"),
minify = require("express-minify"),
path = require("path")

const PORT = process.env.PORT || 8080

var staticPath = path.join(__dirname, 'static')

var app = express()

app.use(minify())
app.use(express.static(staticPath))

app.listen(PORT, () => {
    console.log("The Univerter Foundation Page is now listening on port: " + PORT)
})